from setuptools import setup

setup(
    name="django-bradshaw",
    version="1.0.0",
    description="Mobile notifications for muskego hitmen",
    author="Eric Satterwhite",
    author_email="eric@codedependant.net",
    packages=[ "bradshaw" ],
    classifiers=[
        "Framework::Django",
        "Programming Language::Python2.7"
    ]
)
