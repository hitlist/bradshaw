from django.conf.urls.defaults import *


urlpatterns = patterns('bradshaw.views',
    url(r'^$','login', name='bradshaw_login'),
    url(r'^updates/$', 'game_updates', name='bradshaw_updates'),
    url(r'^nogames/$', 'no_game_update',name='bradshaw_nogames'),
    url(r'^forms/$', 'formtest', name="form_test")
)
