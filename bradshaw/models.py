from django.db import models
from django.db.models import permalink
from django.contrib.auth.models import User
from hitmen.leaguemanager.models import Team, LeagueGame
from sms.models import Carrier
from datetime import datetime

_QUARTERS = (
     (1, '1st Quarter'),
     (2, '2nd Quarter'),
     (3, '3rd Quarter'),
     (4, '4th Quarter'),
     (5, 'Overtime'),
     (6, 'Final')
)
class Subscription(models.Model):
    
    team =      models.ForeignKey(Team)
    user =      models.ForeignKey(User)
    
    phone =     models.CharField(max_length=12, blank=False, null=False)
    carrier =   models.ForeignKey(Carrier,help_text='xxx-xxx-xxxx')
    created =   models.DateTimeField(default=datetime.now, editable=False)

    

class Update(models.Model):
    game =          models.ForeignKey(LeagueGame, null=False, related_name="liveupdates")
    quarter =       models.SmallIntegerField(choices=_QUARTERS, default=1, null=False)
    home_score =    models.PositiveSmallIntegerField( blank=False)
    away_score =    models.PositiveSmallIntegerField(blank=False)
    message =       models.CharField(max_length=255, blank=True, null=True)
    created =       models.DateTimeField(default=datetime.now,editable=False)
    
    class Meta:
        get_latest_by='created'
        ordering=('quarter',)
        
    def __unicode__(self):
        return u'%s %s at %s %s-%s-%s' %(self.quarter, 
					self.game.away_team, 
					self.game.home_team, 
					self.game.date_played.month,
					self.game.date_played.day,
					self.game.date_played.year)
    
    def get_quarter(self):
        if int(self.quarter) < 5:
            return 'Q%s' % self.quarter
        elif int(self.quarter) == 5:
            return "OT"
        elif int(self.quarter) == 6:
            return 'FN'
