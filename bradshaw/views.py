from datetime import datetime
from django.http import HttpResponseForbidden, HttpResponse,\
    HttpResponseBadRequest, Http404
from hitmen.leaguemanager.models import LeagueGame, Team
from bradshaw.models import Update, Subscription
from sms.util import send_sms
from sms.models import Carrier, ContentTypePhoneNumber
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render_to_response,  redirect, HttpResponseRedirect
from bradshaw.forms import UpdateForm, TeamLoginForm
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.views.generic.simple import direct_to_template

def login(request):
    team = request.session.get('bradshaw_team') or None
    #if we are trying to login
    if request.POST:
        #find the team in sessions or fetch it from db
        if team is None:
            form = TeamLoginForm(request.POST)
        
            if form.is_valid():
                team = form.save()
                #if we don't have a team code
                if team is None:
                    raise Http404
                else:    
                    request.session['bradshaw_team'] = team
                    request.session.set_expiry(300)
            else:
                return render_to_response('bradshaw/login.html',
                                          {'form':form}, 
                                          context_instance=RequestContext(request))
        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER') or reverse('bradshaw_updates'))
    
    else:
        if team is not None:
            return HttpResponseRedirect(reverse('bradshaw_updates'))
        form = TeamLoginForm()
        
    return render_to_response('bradshaw/login.html', 
                              {'form':form}, 
                              context_instance=RequestContext(request))

def game_updates(request):
    team = request.session.get('bradshaw_team') or None
    if team is None:
        return redirect(reverse('bradshaw_login'))
    else:
        request.session.set_expiry(300)
    today = datetime.now()
#    game = LeagueGame.objects.all().filter(home_team__pk=1)[0]
    try:
        game = LeagueGame.objects.filter(home_team=team,
                                         date_played__year=today.year,
                                         date_played__month=today.month,
                                         date_played__day=todaye.day
                                         )
    except:
        return HttpResponseRedirect(reverse('bradshaw_nogames'))
    updates = game.liveupdates.all()
    if request.POST:
        form = UpdateForm(request.POST)
        if form.is_valid():
            obj = form.save()
            if request.is_ajax():
                return HttpResponse(simplejson.dumps(dict(
                                        home_score=obj.home_score,
                                        home_logo=game.home_team.helmet_graphic_mobile.url or "",
                                        
                                        away_score=obj.away_score,
                                        away_logo=game.away_team.helmet_graphic_mobile.url or "",
                                        
                                        quarter=obj.get_quarter()
                                    )),
                                    mimetype="application/javascript")
            else:
                return HttpResponseBadRequest()
    else:
         
        form = UpdateForm(initial={'game':game.pk, 'team':team.pk})
    
    return render_to_response('bradshaw/updates.html', 
                              {'form':form,'updates':updates, 'game':game}, 
                              context_instance=RequestContext(request))
        

def no_game_update(request):
    return direct_to_template(request, 'bradshaw/no_update.html')

def formtest(request):

    from bradshaw.forms import SubscriptionForm
    team = request.session.get('bradshaw_team') or None
    if team is None:
        return redirect(reverse('bradshaw_login'))
    
    if request.POST:
        form = SubscriptionForm(request.POST)
        if form.is_valid():
            
            HttpResponseRedirect(team.get_absolute_url())
        else:
            pass
    else:    
        form = SubscriptionForm(initial={
                                         'content_type':ContentType.objects.get_for_model(request.user).pk, 
                                         'object_id':request.user.pk
                                         }
                             )
    return render_to_response('bradshaw/subscribe.html',{'form':form}, context_instance=RequestContext(request))