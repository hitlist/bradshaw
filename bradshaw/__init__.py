from bradshaw.signals import game_update_was_saved, subscription_was_saved
from sms.models import ContentTypePhoneNumber
from sms.util import send_sms
from hitmen.leaguemanager.models import Team
from django.conf import settings
from django.template.loader import render_to_string

def gameupdate(sender, **kwargs):
    update = kwargs.get('update') or None
    tid = kwargs.get('team_id') or None
    team = Team.objects.select_related().get(pk=tid)
    subscribers = team.subscribers.all()
    sub_list = [item for item in subscribers]
    message = render_to_string('bradshaw/text_message.txt', {'update':update,'team':team})
    send_sms(msg=message, 
             from_address=settings.DEFAULT_FROM_EMAIL, 
             recipient_list=sub_list, 
             fail_silently=True)
    
    
game_update_was_saved.connect(gameupdate)