from django import forms
from bradshaw.models import Update,Subscription,_QUARTERS
from bradshaw.signals import game_update_was_saved, subscription_was_saved
from django.contrib.auth.models import User
from hitmen.leaguemanager.models import LeagueGame,Team
from sms.models import Carrier, ContentTypePhoneNumber, ContentType
from datetime import date

class UpdateForm(forms.ModelForm):
    quarter = forms.ChoiceField(choices=_QUARTERS)
    games = LeagueGame.objects.filter(date_played__year=date.today().year)
    game = forms.ModelChoiceField(games, widget=forms.HiddenInput())
    quarter = forms.ChoiceField(choices=_QUARTERS)
    team = forms.IntegerField(widget=forms.HiddenInput())
    
    
    class Meta:
        model = Update
        
    def save(self):
        u = super(UpdateForm, self).save()
        game_update_was_saved.send(sender=self,update=u, team_id=self.cleaned_data['team'])
        return u


class TeamLoginForm(forms.Form):
    team_code = forms.CharField(max_length=100)
    
    def save(self):
        data = self.cleaned_data        
        try:
            team = Team.objects.get(team_code=data.get('team_code'))
        except:
            team = None        
        return team

class SubscriptionForm(forms.ModelForm):
    types = ContentType.objects.all()
    content_type = forms.ModelChoiceField(types, widget=forms.HiddenInput())
    object_id = forms.IntegerField(widget=forms.HiddenInput())
  
  
    class Meta:
        model = ContentTypePhoneNumber
        exclude=('is_primary_number','description')


