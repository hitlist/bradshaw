# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Update.quarter'
        db.alter_column('bradshaw_update', 'quarter', self.gf('django.db.models.fields.SmallIntegerField')())


    def backwards(self, orm):
        
        # Changing field 'Update.quarter'
        db.alter_column('bradshaw_update', 'quarter', self.gf('django.db.models.fields.PositiveSmallIntegerField')())


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'bradshaw.subscription': {
            'Meta': {'object_name': 'Subscription'},
            'carrier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sms.Carrier']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '12'}),
            'team': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['leaguemanager.Team']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'bradshaw.update': {
            'Meta': {'ordering': "('quarter',)", 'object_name': 'Update'},
            'away_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'liveupdates'", 'to': "orm['leaguemanager.LeagueGame']"}),
            'home_score': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'quarter': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'leaguemanager.conference': {
            'Meta': {'object_name': 'Conference'},
            'conference_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageWithThumbnailsField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'})
        },
        'leaguemanager.leaguegame': {
            'Meta': {'ordering': "['-date_played']", 'object_name': 'LeagueGame'},
            'away_chp_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_chp_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_ply_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_ply_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_pre_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_pre_tie': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_pre_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_reg_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_reg_tie': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'away_reg_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'away_score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'away_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'awayteam'", 'to': "orm['leaguemanager.Team']"}),
            'date_played': ('django.db.models.fields.DateTimeField', [], {}),
            'during_season': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['leaguemanager.Season']"}),
            'game_type': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'home_chp_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_chp_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_ply_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_ply_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_pre_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_pre_tie': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_pre_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_reg_loss': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_reg_tie': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'home_reg_win': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'home_score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'home_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'hometeam'", 'to': "orm['leaguemanager.Team']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'leaguemanager.season': {
            'Meta': {'ordering': "['-season_year']", 'object_name': 'Season'},
            'active_teams': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['leaguemanager.Team']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'season_year': ('django.db.models.fields.DateField', [], {})
        },
        'leaguemanager.stadium': {
            'City': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Meta': {'object_name': 'Stadium'},
            'State': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'Zip': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'consessions': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'field_type': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lights': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'stadium_name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'leaguemanager.team': {
            'Meta': {'object_name': 'Team'},
            'conference': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['leaguemanager.Conference']"}),
            'default_team': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'helmet_graphic_away': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'helmet_graphic_home': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'helmet_graphic_mobile': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'hits': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'home_field': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['leaguemanager.Stadium']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'subscribers': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['sms.ContentTypePhoneNumber']", 'null': 'True', 'blank': 'True'}),
            'team_bio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'team_city': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'team_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'team_gallery': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['photologue.Gallery']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'team_head_coach': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'team_logo': ('sorl.thumbnail.fields.ImageWithThumbnailsField', [], {'max_length': '100'}),
            'team_name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'team_nickname': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'team_owner': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'team_rivals': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'rival_set'", 'blank': 'True', 'null': 'True', 'to': "orm['leaguemanager.Team']"}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'year_established': ('django.db.models.fields.DateField', [], {})
        },
        'photologue.gallery': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Gallery'},
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'galleries'", 'blank': 'True', 'null': 'True', 'to': "orm['photologue.Photo']"}),
            'tags': ('tagging.fields.TagField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True'}),
            'title_slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'unique': 'True', 'db_index': 'True'})
        },
        'photologue.photo': {
            'Meta': {'ordering': "['-date_added']", 'object_name': 'Photo'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'crop_from': ('django.db.models.fields.CharField', [], {'default': "'center'", 'max_length': '10', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_taken': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'effect': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photo_related'", 'blank': 'True', 'null': 'True', 'to': "orm['photologue.PhotoEffect']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'tags': ('tagging.fields.TagField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True'}),
            'title_slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'unique': 'True', 'db_index': 'True'}),
            'view_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'photologue.photoeffect': {
            'Meta': {'object_name': 'PhotoEffect'},
            'background_color': ('django.db.models.fields.CharField', [], {'default': "'#FFFFFF'", 'max_length': '7'}),
            'brightness': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'color': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'contrast': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'filters': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'}),
            'reflection_size': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'reflection_strength': ('django.db.models.fields.FloatField', [], {'default': '0.59999999999999998'}),
            'sharpness': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'transpose_method': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        'sms.carrier': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('name', 'gateway'),)", 'object_name': 'Carrier'},
            'gateway': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'sms.contenttypephonenumber': {
            'Meta': {'ordering': "('content_type', '-carrier', '-is_primary_number')", 'unique_together': "(('content_type', 'object_id', 'phone_number'), ('content_type', 'object_id', 'phone_number', 'is_primary_number'))", 'object_name': 'ContentTypePhoneNumber'},
            'carrier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phone_numbers'", 'to': "orm['sms.Carrier']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phone_numbers'", 'to': "orm['contenttypes.ContentType']"}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_primary_number': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['bradshaw']
