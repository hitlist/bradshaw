from django.dispatch import Signal

game_update_was_saved = Signal(providing_args=['update','team_id'])
subscription_was_saved = Signal(providing_args=['subscription',])
